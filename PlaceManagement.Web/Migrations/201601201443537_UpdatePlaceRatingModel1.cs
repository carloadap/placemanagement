namespace PlaceManagement.Web.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UpdatePlaceRatingModel1 : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.PlaceRatings", "Place_Id", "dbo.Places");
            DropIndex("dbo.PlaceRatings", new[] { "Place_Id" });
            RenameColumn(table: "dbo.PlaceRatings", name: "Place_Id", newName: "PlaceId");
            AlterColumn("dbo.PlaceRatings", "PlaceId", c => c.Int(nullable: false));
            CreateIndex("dbo.PlaceRatings", "PlaceId");
            AddForeignKey("dbo.PlaceRatings", "PlaceId", "dbo.Places", "Id", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.PlaceRatings", "PlaceId", "dbo.Places");
            DropIndex("dbo.PlaceRatings", new[] { "PlaceId" });
            AlterColumn("dbo.PlaceRatings", "PlaceId", c => c.Int());
            RenameColumn(table: "dbo.PlaceRatings", name: "PlaceId", newName: "Place_Id");
            CreateIndex("dbo.PlaceRatings", "Place_Id");
            AddForeignKey("dbo.PlaceRatings", "Place_Id", "dbo.Places", "Id");
        }
    }
}
