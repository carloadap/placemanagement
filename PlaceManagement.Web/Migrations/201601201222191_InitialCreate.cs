namespace PlaceManagement.Web.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialCreate : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.PlaceRatings",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Comments = c.String(maxLength: 200),
                        Ratings = c.Int(nullable: false),
                        Place_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Places", t => t.Place_Id)
                .Index(t => t.Place_Id);
            
            CreateTable(
                "dbo.Places",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false),
                        Description = c.String(nullable: false, maxLength: 150),
                        CreatedDateTime = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.PlaceRatings", "Place_Id", "dbo.Places");
            DropIndex("dbo.PlaceRatings", new[] { "Place_Id" });
            DropTable("dbo.Places");
            DropTable("dbo.PlaceRatings");
        }
    }
}
