namespace PlaceManagement.Web.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UpdatePlaceRatingModel : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.PlaceRatings", "Username", c => c.String(nullable: false, maxLength: 50));
        }
        
        public override void Down()
        {
            DropColumn("dbo.PlaceRatings", "Username");
        }
    }
}
