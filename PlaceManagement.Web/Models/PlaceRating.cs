﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace PlaceManagement.Web.Models
{
    public class PlaceRating
    {
        [Key]
        public int Id { get; set; }

        [StringLength(200)]
        public string Comments { get; set; }

        [Required]
        [Range(1,10)]
        public int Ratings { get; set; }

        [Required]
        [StringLength(50)]
        public string Username { get; set; }

        [ForeignKey("Place")]
        public int PlaceId { get; set; }

        public virtual Place Place { get; set; }
            
    }
}