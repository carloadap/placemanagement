﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace PlaceManagement.Web.Models
{
    public class Place
    {
        [Key]
        public int Id { get; set; }

        [Required]
        public string Name { get; set; }

        [Required]
        [StringLength(150)]
        public string Description { get; set; }

        [DisplayName("Created Date")]
        [DataType(DataType.Date)]
        [Required]
        public DateTime CreatedDateTime { get; set; }

        public virtual ICollection<PlaceRating> PlaceRatings { get; set; }

    }
}