﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using PlaceManagement.Web.Models;

namespace PlaceManagement.Web.DAL
{
    public class PlaceDbContext : DbContext
    {
        public PlaceDbContext() : base("DbConnectionString") { }

        public DbSet<Place> Places { get; set; }
        public DbSet<PlaceRating> PlaceRatings { get; set; }
    }
}