﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using PlaceManagement.Web.DAL;
using PlaceManagement.Web.Models;

namespace PlaceManagement.Web.Tests.Models
{
    [TestClass]
    public class PlaceTest
    {
        [TestMethod]
        public void Create_Place()
        {
            using (var context = new PlaceDbContext())
            {
                context.Places.Add(new Place
                {
                    Name = "Ayala",
                    Description = "Sample Description",
                    CreatedDateTime = DateTime.Now
                
                });
                context.SaveChanges();

                foreach (var person in context.Places)
                {
                    Console.WriteLine(person.Name);
                }
            }
        }
    }
}
